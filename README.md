# HikariCPConnection

Usage:
```java
AbstractConnection h2Connection = new H2Connection(new File("C:/mybase"));

try(Connection connection = h2Connection.getConnection()) {
    //your query
} catch(SQLException e) {
    e.printStackTrace();
}
```