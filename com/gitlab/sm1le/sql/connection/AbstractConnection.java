package com.gitlab.sm1le.sql.connection;

import com.zaxxer.hikari.HikariDataSource;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public abstract class AbstractConnection {

    HikariDataSource hikariDataSource;

    AbstractConnection(String url, String user, String password) {
        this.hikariDataSource = new HikariDataSource();
        this.hikariDataSource.setJdbcUrl(url);
        this.hikariDataSource.setUsername(user);
        this.hikariDataSource.setPassword(password);
    }

    public Connection getConnection() throws SQLException {
        return hikariDataSource.getConnection();
    }
    
    public void close() {
        hikariDataSource.close();
    }

    public abstract String getType();
    
}
