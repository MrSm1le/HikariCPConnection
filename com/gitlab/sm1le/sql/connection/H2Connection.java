package com.gitlab.sm1le.sql.connection;

import java.io.File;

public class H2Connection extends AbstractConnection {


    public H2Connection(File targetFile) {
        super("jdbc:h2:" + targetFile.getAbsolutePath(), "h2Admin", "");
    }

    public String getType() {
        return "H2";
    }
    
}